$(document).ready(function (e) {
    $("#form1").on('submit',(function(e) {
        e.preventDefault();
        $.ajax({
            url: "profile.php",
            type: "POST",
            data:  new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            beforeSend : function()
            {
                //$("#preview").fadeOut();
                $("#err").fadeOut(1000);
            },
            success: function(data)
            {
                if(data=='invalid')
                {
                    // invalid file format.
                    $("#err").html("Invalid File !").fadeIn();
                }
                else
                {
                    // view uploaded file.
                    $("#preview").html(data).fadeIn();
                    $("#form")[0].reset();
                }
            },
            error: function(e)
            {
                $("#err").html(e).fadeIn();
            }
        });
    }));
});






$(document).ready(function() {
    $('#submit').click(function(e) {
        e.preventDefault();
        // validation
        $.ajax({
            type: "POST",
            url: 'submit.php',
            data:  $('#form').serialize(),
            dataType: 'json',
            cache: false,
            success: function(result) {
                $('#form').html("<div id='message'></div>");
                $('#message').html("<h2>Contact Form Submitted!</h2>")
                    .append("<p>We will be in touch soon.</p>")
                    .hide()
                    .fadeIn(1500, function (){})
            },

            error: function (response, desc, exception) {
                alert(Error)
            },
            beforeSend: function() {
                $('#loader').fadeIn(1000);
            },
            complete: function() {
                $('#loader').fadeOut(1000);
            },
        });
    });
});
