
<?php
// including the database connection file
include_once("db_connection.php");

include 'functions.php';

if(isset($_POST['update']))
{
    $id = filter_input(INPUT_POST, 'id');

    $fname=filter_input(INPUT_POST, 'fname');
    $lname=filter_input(INPUT_POST, 'lname');
    $address=filter_input(INPUT_POST, 'address');
    $city=filter_input(INPUT_POST, 'city');
    $state=filter_input(INPUT_POST, 'state');
    $zip=filter_input(INPUT_POST, 'zip');
    $profile_pic=filter_input(INPUT_POST, 'profile_pic');
    $param_password = hash('sha256', filter_input(INPUT_POST, 'password'));

    filter_input(INPUT_POST, '');
    // checking empty fields
    if(empty($fname) || empty($lname) || empty($address) || empty($city) || empty($state) || empty($zip)){

        if(empty($fname)) {
            echo "<font color='red'>First Name field is empty.</font><br/>";
        }

        if(empty($lname)) {
            echo "<font color='red'>Last Name field is empty.</font><br/>";
        }

        if(empty($address)) {
            echo "<font color='red'>Address field is empty.</font><br/>";
        }
        if(empty($city)) {
            echo "<font color='red'>City field is empty.</font><br/>";
        }
        if(empty($state)) {
            echo "<font color='red'>State field is empty.</font><br/>";
        }
        if(empty($zip)) {
            echo "<font color='red'>Zip field is empty.</font><br/>";
        }

    } else {
        //updating the table
        $sql = "UPDATE user SET first_name =:fname, last_name =:lname, address =:address state=:state, zip=:zip, profile_pic = :profile_pic WHERE id=:id";
        $query = $db->prepare($sql);

        $query->bindparam(':id', $id);
        $query->bindparam(':fname', $fname);
        $query->bindparam(':lname', $lname);
        $query->bindparam(':address', $address);
        $query->bindparam(':city', $city);
        $query->bindparam(':state', $state);
        $query->bindparam(':zip', $zip);
        $query->bindParam(':profile_pic', $profile_pic);
        $query->execute();


        //redirectig to the home page.
        header("Location: index.php");
    }

?>
<?php
//getting id from url
$id = $_POST['id'];

//selecting data associated with this particular id
$sql = "SELECT * FROM user WHERE id=:id";
$query = $db->prepare($sql);
$query->execute(array(':id' => $id));

while($row = $query->fetch(PDO::FETCH_ASSOC))
{
    $fname = $row['fname'];
    $lname = $row['lname'];
    $address = $row['address'];
    $city = $row['city'];
    $state = $row['state'];
    $zip = $row['zip'];
    $profile_pic=$row['profile_pic'];

}
}
?>
<html>
<head>
    <title>Update Profile</title>
</head>

<body>
<a href="#">Home</a>
<br/><br/>

<form name="form1" id="form1"method="post" action="profile.php">
    <table border="0">
        <tr>
            <td>First Name</td>
            <td><input type="text" name="fname" </td>
        </tr>
        <tr>
            <td>Last Name</td>
            <td><input type="text" name="lname"></td>
        </tr>
        <tr>
            <td>Address</td>
            <td><input type="text" name="address"> </td>
        </tr>
        <tr>
            <td>City</td>
            <td><input type="text" name="city"></td>
        </tr>
        <tr>
            <td>State</td>
            <td>
            <select name="state">
                <?php foreach ($states as $key => $value) { ?>
                   <option value="<?php echo $key; ?>">
                       <?php echo $value; ?></option>
                <?php } ?>
            </select>
            </td>
        </tr>
        <tr>
            <td>Zip Code</td>
            <td><input type="text" name="zip" </td>
        </tr>
        <tr>
            <td>Profile Picture</td>
            <td><input type="file" name="profile_pic" value="browse"</td>
        </tr>
        <tr>
            <td>Change Password</td>
            <td><input type="password" name="password" value="browse"</td>
        </tr>
        <tr>
            <td><input type="hidden" name="id" value="<?php echo $_GET['id'];?>"></td>
            <td><input type="submit" name="update" value="Update"></td>
        </tr>
    </table>
</form>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="profile.js"></script>
</body>
</html>
