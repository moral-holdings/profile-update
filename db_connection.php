<?php
///* Connect to a MySQL database using driver invocation */


$dsn = 'mysql:host=localhost;dbname=registration';
$username = 'root';
$password = '';

try {
    $db = new PDO($dsn, $username, $password);
} catch (PDOException $e) {
    $error_message = $e->getMessage();
    include('database_error.php');
    exit();
}

